#!env /bin/python3
# -*- coding:utf-8 -*-
import sys
import os
import multiprocessing
from datetime import datetime, timedelta
import logging

picdir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "pic")
libdir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "lib")
conffile = os.path.join(os.path.dirname(os.path.realpath(__file__)), "tank_config.yaml")

if os.path.exists(libdir):
    sys.path.append(libdir)

from waveshare_epd import epd2in7
import time
from PIL import Image,ImageDraw,ImageFont
import traceback
import yaml
from gpiozero import Button
from signal import pause
from influxdb import InfluxDBClient

#logging.basicConfig(level=logging.INFO)
logging.basicConfig(level=logging.DEBUG)

# Fonts
font10 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 10)
font14 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 14)
font20 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 20)
font24 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 24)

# Open config file
try:
  with open(conffile, 'r') as file:
    tank_config = yaml.full_load(file)
except FileNotFoundError:
  print("Configuration file tank_config.yaml missing.")
  print("Please use tank_config_example.yaml as a template.")
  sys.exit()

# Get configuration
logging.debug("Reading configuration")

tanks = tank_config.get("tank")
title = tank_config.get("config").get("title")
refresh = tank_config.get("config").get("refresh")
timezone = tank_config.get("config").get("timezone")
strptime_slice = tank_config.get("config").get("strptime_slice")
align_right_offset = 260

logging.debug("Init and clear display")
epd = epd2in7.EPD()
epd.init()
#epd.Clear(0xFF)

def draw_stock(tanks, refresh):
  try:
    data_error = False
    data_age = "✓"
    voffset = 35
    hoffset = 2
    Himage = Image.new('1', (epd.height, epd.width), 255)  # 255: clear the frame
    draw = ImageDraw.Draw(Himage)

    #Title
    draw.text((0, 0), str(title), font = font24, fill = 0)
    draw.line((0, 30, 264, 30), fill = 0)

    #Timestamp
    dcontent = datetime.now().strftime("%d.%m.%Y %H:%M")
    wd, hd = draw.textsize(dcontent, font=font14)
    draw.text((align_right_offset - wd, 0), str(dcontent), font = font14, fill = 0)

    logging.debug("Starting tank display loop")
    for tankname in tanks:
      logging.debug("Looping for " + tankname)

      #Get latest measurement from influxdb
      client = InfluxDBClient(host='localhost', port=8086)
      client.switch_database('tanks')
      influxdata = client.query('''SELECT "value" FROM distance WHERE "tank" = '%s' GROUP BY * ORDER BY DESC LIMIT 1 tz('%s')''' % (tankname, timezone))
      logging.debug(influxdata)
      try:
        distance = influxdata.raw['series'][0]['values'][0][1]
        timestamp = influxdata.raw['series'][0]['values'][0][0]
      except KeyError:
        data_error = True
        distance = "No data available!"
        pass

      #Calculate age of metric
      metrictime = datetime.strptime(timestamp[:-strptime_slice], "%Y-%m-%dT%H:%M:%S.%f")
      metrictimedelta = datetime.now() - metrictime
      logging.debug("Metric age: " + str(metrictime))
      logging.debug("Metric age delta: " + str(metrictimedelta))
      if metrictimedelta > timedelta(minutes = 30):
        data_age = "×"

      #Get tank infos depth from config
      depth = tank_config.get("tank")[tankname]["depth"]
      capacity = tank_config.get("tank")[tankname]["capacity"]
      try:
        offset = tank_config.get("tank")[tankname]["offset"]
      except KeyError:
        offset = 0

      #Sanity check
      if distance >= depth:
        data_error = True
        distance = "Measurement error!"
        pass

      #Calculate current level
      if not data_error:
        logging.debug("depth: " + str(depth) + ", distance: " + str(distance) + ", offset: " + str(offset))
        current_level_percent = round((depth - (distance - offset)) * 100 / depth, 2)
        current_level_liters = round(current_level_percent * capacity / 100)

        #Put infos together for display
        tankstatus = tankname + ": " + str(current_level_percent) + "% (ca. " + str(current_level_liters) + "L) " + data_age
      else:
        tankstatus = tankname + ": " + str(distance)

      draw.text((0, voffset), tankstatus, font = font20, fill = 0)

      #Draw tank image
      if not data_error:
        draw.line((0, 115, 264, 115), fill = 0)
        draw.text((hoffset, 120), tankname, font = font10, fill = 0)
        draw.rectangle((hoffset, 134, (hoffset + 70), 174), fill = 0)
        empty_percent = 100 - current_level_percent
        draw.rectangle(((hoffset + 2), 136, (hoffset + 68), (136 + (36 * empty_percent / 100))), fill = 1)

      voffset += 26
      hoffset += 90

  except IOError as e:
      logging.info(e)
      
  except KeyboardInterrupt:    
      logging.info("ctrl + c:")
      epd2in7.epdconfig.module_exit()
      exit()
      
  epd.display(epd.getbuffer(Himage))
  logging.debug("Sleeping: " + str(refresh))
  time.sleep(refresh)

def display_loop(tanks, refresh,):
  logging.debug("Running main display loop")
  draw_stock(tanks, refresh)

#######################################################

def handleBtn1():
    global btn4_press
    logging.debug("Button 1 pressed")
    btn4_press = False

def handleBtn2():
    global btn4_press
    logging.debug("Button 2 pressed")
    btn4_press = False

def handleBtn3():
    global btn4_press
    logging.debug("Button 3 pressed")
    btn4_press = False

def handleBtn4():
    global btn4_press
    logging.debug("Button 4 pressed")
    if btn4_press == True:
        logging.debug("Button 4 second press")
        logging.info("Starting shutdown")
        os.system("poweroff")
    else:
        logging.debug("Button 4 first press")
        btn4_press = True

def button_loop(q):
  global btn4_press

  btn1 = Button(5)
  btn2 = Button(6)
  btn3 = Button(13)
  btn4 = Button(19)

  btn4_press = False

  btn1.when_pressed = handleBtn1
  btn2.when_pressed = handleBtn2
  btn3.when_pressed = handleBtn3
  btn4.when_pressed = handleBtn4

  logging.info("Button handling initialized")
  # Without pause it would exit and not handle buttons ;)
  pause()

def setbuttonstatus(q, button, status):
  try:
    buttonstatus = q.get_nowait()
  except Empty:
    buttonstatus = [False, False, False, False]
  buttonstatus[button] = status
  q.put(buttonstatus)

def getbuttonstatus(q, button):
  try:
    buttonstatus = q.get_nowait()
  except Empty:
    buttonstatus = [False, False, False, False]
  q.put(buttonstatus)
  return(buttonstatus[button])

#######################################################

q = multiprocessing.Queue()
displayprocess = multiprocessing.Process(target=display_loop, args=(tanks, refresh,))
buttonprocess = multiprocessing.Process(target=button_loop, args=(q,))

if __name__ == '__main__':
  displayprocess.start()
  buttonprocess.start()
  while True:
    if displayprocess.is_alive() == False:
      logging.error("Display process crashed. See log for details. Restarting.")
      displayprocess = multiprocessing.Process(target=display_loop, args=(tanks, refresh,))
      displayprocess.start()
    time.sleep(60)
